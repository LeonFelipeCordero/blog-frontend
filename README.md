# blog-frontend

## Project setup
```
npm install / yarn
```

### Compiles and hot-reloads for development
```
npm run serve / yarn serve
```

### Compiles and minifies for production
```
npm run build / yarn build
```

### run on k8s
Before deploying the blog-frontend to a k8s-cluster make sure you follow the instruction in https://gitlab.com/LeonFelipeCordero/blog-api about how to create the k8s environment.

One mongodb and blog-api are running you can deploy the frontend

```
kubectl apply -f ci/k8s/deployment.yml
```