export default class BlogApiClient {

    constructor() {
        this.url = process.env.VUE_APP_BLOG_API_HOST || 'http://localhost:8080'
        this.endpoints = {
            profile: {
                profile: '/profile',
                profileId: '/profile/$id',
                toAuthor: '/profile/$id/author'
            },
            article: {
                article: '/article',
                articleId: '/article/$id',
                articlesByProfile: '/article?author=$author',
                comment: '/article/$id/comment',
                top: '/article/top'
            },
            login: {
                login: '/login'
            },
            contactUs: {
                contactUs: '/contact-us'
            }
        }
        this.headers = {
            'Content-Type': 'application/json',
        }
    }

    async getProfile(profileId) {
        return await this.get(this.url + this.endpoints.profile.profileId.replace('$id', profileId))
    }

    async updateProfile(profileId, request) {
        return await this.put(
            this.url + this.endpoints.profile.profileId.replace('$id', profileId),
            request
        )
    }

    async createProfile(request) {
        return await this.post(
            this.url + this.endpoints.profile.profile,
            request
        )
    }

    async deleteProfile(profileId) {
        return await this.delete(this.url + this.endpoints.profile.profileId.replace('$id', profileId))
    }

    async updateToAuthor(profileId) {
        const response = await fetch(this.url + this.endpoints.profile.toAuthor.replace('$id', profileId), {
            method: 'PUT',
            headers: this.headers
        })
        return await response.json()
    }

    async getArticlesByAuthor(profileId) {
        return await this.get(this.url + this.endpoints.article.articlesByProfile.replace('$author', profileId))
    }

    async getArticleById(articleId) {
        return await this.get(this.url + this.endpoints.article.articleId.replace('$id', articleId))
    }

    async createArticle(request) {
        return await this.post(
            this.url + this.endpoints.article.article,
            request
        )
    }

    async updateArticle(articleId, request) {
        return await this.put(
            this.url + this.endpoints.article.articleId.replace('$id', articleId),
            request
        )
    }

    async addComment(articleId, request) {
        return await this.put(
            this.url + this.endpoints.article.comment.replace('$id', articleId),
            request
        )
    }

    async getTopArticles() {
        return await this.get(this.url + this.endpoints.article.top)
    }

    async login(request) {
        return await this.put(
            this.url + this.endpoints.login.login,
            request
        )
    }

    async contactUs(request) {
        return await this.post(
            this.url + this.endpoints.contactUs.contactUs, request
        )
    }

    async get(url) {
        const response = await fetch(url, {
            method:'GET',
            headers: this.headers
        })
        return await response.json()
        // const response = await axios.get(url, {
        //     headers: this.headers
        // })
        // return response.data
    }

    async post(url, request) {
        const response = await fetch(url, {
            method:'POST',
            headers: this.headers,
            body: JSON.stringify(request)
        })
        return await response.json()
        // const response = await axios.post(url, request, {
        //     headers: this.headers
        // })
        // return response.data
    }

    async put(url, request) {
        const response = await fetch(url, {
            method:'PUT',
            headers: this.headers,
            body: JSON.stringify(request)
        })
        return await response.json()
        // const response = await axios.put(url, request, {
        //     headers: this.headers
        // })
        // return response.data
    }

    async delete(url) {
        const response = await fetch(url, {
            method:'DELETE',
            headers: this.headers
        })
        return await response.json()
        // const response = await axios.delete(url, {
        //     headers: this.headers
        // })
        // return response.data
    }
}