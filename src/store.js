import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        logged: false,
        profileData: Object
    },
    plugins: [createPersistedState()],
    mutations: {
        //todo this.state
        LOGIN: (state) => {
            state.logged = true
        },
        LOGOUT: (state) => {
            state.logged = false
        },
        SET_PROFILE_DATA: (state, profileData) => {
            state.profileData = profileData
        },
    },
    actions: {
        login: ({commit}) => {
            commit('LOGIN')
        },
        logout: ({commit}) => {
            commit('LOGOUT')
        },
        setProfileData: ({commit}, profileData) => {
            commit('SET_PROFILE_DATA', profileData)
        }
    },

})
