import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/LogIn.vue'
import Signup from './views/SignUp.vue'
import Profile from './views/Profile.vue'
import ArticleEditor from './views/ArticleEditor'
import Article from './views/Article'
import TermsAndConditions from './views/TermsAndConditions'
import UpdateProfile from './views/UpdateProfile'
import ContactUs from './views/ContactUs'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/signup',
            name: 'signup',
            component: Signup
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/article-editor',
            name: 'article-editor',
            component: ArticleEditor
        },
        {
            path: '/article/:id',
            name: 'article',
            component: Article
        },
        {
            path: '/terms-and-conditions',
            name: 'TermsAndConditions',
            component: TermsAndConditions
        },
        {
            path: '/profile/update',
            name: 'UpdateProfile',
            component: UpdateProfile
        },
        {
            path: '/contact-us',
            name: 'ContactUs',
            component: ContactUs
        }
    ]
})
