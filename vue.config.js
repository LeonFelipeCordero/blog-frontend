const webpack = require("webpack");
const BundleTracker = require("webpack-bundle-tracker");

module.exports = {
  // publicPath: '/',
  outputDir: "./dist/",
  filenameHashing: false,

  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      })
    ]
  },

  chainWebpack: config => {
    config
      .plugin("BundleTracker")
      .use(BundleTracker, [{ filename: "./webpack-stats.json" }]);

    config.output.filename("bundle.js");

    config.optimization.splitChunks(false);

    config.optimization.delete("splitChunks");

    config.resolve.alias.set("__STATIC__", "static");

    config.devServer
      .hotOnly(true)
      .watchOptions({ poll: 1000 })
      .https(false)
      .disableHostCheck(true)
      .headers({ "Access-Control-Allow-Origin": ["*"] });
  },

  devServer: {
    proxy: process.env.VUE_APP_BLOG_API_HOST || 'http://localhost:8080'
  },

  css: {
    extract: {
      filename: "bundle.css",
      chunkFilename: "bundle.css"
    }
  }
};